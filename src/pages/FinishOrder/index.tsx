import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Feather } from '@expo/vector-icons'

import { useNavigation, useRoute, RouteProp } from '@react-navigation/native'
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { StackParamsList } from "../../routes/app.routes";
import { api } from "../../services/api";

type RouteDetailsParam = {
  FinishOrder: {
    number: number | string
    order_id: string
  }
}

type FinishOrderProp = RouteProp<RouteDetailsParam, 'FinishOrder'>

export function FinishOrder() {
  const route = useRoute<FinishOrderProp>()
  const navigation = useNavigation<NativeStackNavigationProp<StackParamsList>>()

  async function handleFinish() {
    try {
      await api.put('/order/draft/', {
        order_id: route.params?.order_id
      })

      navigation.popToTop()
    } catch (err) {
      console.log('Erro ao finalizar!', err)
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.alert} >Você deseja finalizar esse pedido?</Text>
      <Text style={styles.title}>Mesa {route.params?.number}</Text>

      <TouchableOpacity style={styles.button} onPress={handleFinish}>
        <Text style={styles.textButton}>Finalizar pedido</Text>
        <Feather name="shopping-cart" size={20} color="#1d1d2e" />
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: '4%',
    paddingVertical: '5%',
    backgroundColor: '#1d1d2e'
  }, alert: {
    fontSize: 20,
    color: '#fff',
    fontWeight: 'bold',
    marginBottom: 12
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 12
  },
  button:{
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3fffa3',
    width: '65%',
    height: 40,
    borderRadius: 4
  },
  textButton: {
    fontSize: 18,
    marginRight: 8,
    fontWeight: 'bold',
    color: '#1d1d2e'
  }
})