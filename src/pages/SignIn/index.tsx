import { useContext, useState } from "react";
import { ActivityIndicator, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";

import { AuthContext } from "../../context/AuthContext";

export default function SignIn() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const { SignIn, loadingAuth } = useContext(AuthContext)

  async function handleLogin() {
    if (email === '' || password === '') {
      return
    }
    
    await SignIn({ email, password })
  }

  return (
    <View style={styles.container}>
      <Image source={require('../../assets/logo.png')} style={styles.logo} />
      
      <View style={styles.inputContainer}>
        <TextInput 
          style={styles.input} 
          placeholder="Digite seu email" 
          placeholderTextColor='#f0f0f0'
          value={email}
          onChangeText={setEmail}
        />

        <TextInput 
          style={styles.input}  
          placeholder="Digite sua senha" 
          placeholderTextColor='#f0f0f0'
          value={password}
          onChangeText={setPassword}
        />

        <TouchableOpacity style={styles.button} onPress={handleLogin}>
          {
            loadingAuth ? (
              <ActivityIndicator size={25} color='#fff' />
            ) : (
              <Text style={styles.buttonText}>Acessar</Text>
            )
          }
        </TouchableOpacity>
      </View>

    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#1d1d2e'
  },
  logo: {
    marginBottom: 18
  },
  inputContainer: {
    width: '95%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  input: {
    width: '95%',
    height: 40,
    backgroundColor: '#101026',
    marginBottom: 12,
    borderRadius: 4,
    paddingHorizontal: 8,
    color: '#fff'
  },
  button: {
    width: '95%',
    height: 40,
    backgroundColor: '#3fffa3',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#101026'
  }
})