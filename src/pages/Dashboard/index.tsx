import { useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { useContext, useState } from "react";
import { SafeAreaView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { MaterialIcons } from '@expo/vector-icons'

import { AuthContext } from "../../context/AuthContext";
import { StackParamsList } from "../../routes/app.routes";
import { api } from "../../services/api";



export default function Dashboard() {
  const [number, setNumber] = useState('')

  const navigation = useNavigation<NativeStackNavigationProp<StackParamsList>>()

  const { SignOut } = useContext(AuthContext)

  async function openOrder() {
    if (number === '') {
      return
    }

    const response = await api.post('/order', {
      table: Number(number)
    })
    
    const { id } = response.data

    navigation.navigate('Order', { number: number, order_id: id })

    setNumber('')
  }

  return (
    <>
    <View style={styles.logOut}>
      <TouchableOpacity onPress={SignOut} style={styles.logOutButton}>
        <MaterialIcons name="logout" size={24} color="#fff" />
      </TouchableOpacity>
    </View>
    
    <SafeAreaView style={styles.container}>
      <Text style={styles.title}>Novo pedido</Text>

      <TextInput 
        placeholder="Numero da mesa"
        placeholderTextColor='#f0f0f0'
        keyboardType="numeric"
        style={styles.input}
        value={number}
        onChangeText={setNumber}
      />

      <TouchableOpacity style={styles.button} onPress={openOrder}>
        <Text style={styles.buttonTex}>Abrir mesa</Text>
      </TouchableOpacity>
    </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
    backgroundColor: '#1d1d2e'
  }, 
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 24
  },
  input: {
    width: '90%',
    height: 60,
    backgroundColor: '#101026',
    borderRadius: 4,
    paddingHorizontal: 8,
    textAlign: 'center'
  },
  button: {
    width: '90%',
    height: 40,
    backgroundColor: '#3fffa3',
    borderRadius: 4,
    marginVertical: 12,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonTex: {
    fontSize: 18,
    color: '#101026',
    fontWeight: 'bold'
  },
  logOut: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#1d1d2e'
  },
  logOutButton: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingHorizontal: 15,
    paddingVertical: 10
  }
})