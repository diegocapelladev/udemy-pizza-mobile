# Pizza Project Mobile

## Getting Started

```bash
npm run start
# or
yarn start
```
Scan the QR code above with Expo Go (Android) or the Camera app (iOS)

## Pizza Project Back-end 

The API can be accessed on [https://gitlab.com/diegocapelladev/udemy-pizza-backend](https://gitlab.com/diegocapelladev/udemy-pizza-backend)

## Pizza Project Front-end 

The FRONT-END can be accessed on [https://gitlab.com/diegocapelladev/udemy-pizza-frontend](https://gitlab.com/diegocapelladev/udemy-pizza-frontend)

## Preview

<section align="center" width="100%" >
  <img alt="projeto Pizza Project" src=".github/pizza-preview-mobile-1.jpg">
  <img alt="projeto Pizza Project" src=".github/pizza-preview-mobile-2.jpg">
</section>
